{
  "data": [
    {
      "CustomerId": 35066193,
      "Label": "客戶經營",
      "BeginDate": "2006-06-17",
      "Name": "Berger Carter",
      "Gender": "男",
      "Birthday": "1995-02-23",
      "Level": "一般會員",
      "AssignDate": "2018-11-03",
      "LastCallDate": "2018-10-31",
      "ScheduleDate": "2018-11-04",
      "AdviceProduct": ["non", "laboris", "laboris"]
    },
    {
      "CustomerId": 31145561,
      "Label": "人傻錢多速來",
      "BeginDate": "1994-06-11",
      "Name": "Meyer Joyce",
      "Gender": "男",
      "Birthday": "1988-04-25",
      "Level": "一般會員",
      "AssignDate": "2018-10-23",
      "LastCallDate": "2018-10-26",
      "ScheduleDate": "2018-11-10",
      "AdviceProduct": ["qui", "esse", "elit"]
    },
  ]
  
}