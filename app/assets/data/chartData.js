var defaultColor = {
    primaryColor: 'rgb(0,98,204)',
    secondaryColor: 'rgb(84,91,98)',
    successColor: 'rgb(30,126,52)',
    dangerColor: 'rgb(255,57,31)',
    warningColor: 'rgb(211,158,0)',
    columbiaBlueColor: 'rgb(37,167,203)',
    silverColor: 'rgb(226,230,234)',
    steelColor: 'rgb(29,33,36)',
    whiteColor: 'rgb(255,255,255)'
}

var lineChartColor = {
    primaryColorRgba: 'rgba(0,98,204, 1)',
    primaryColorLight: '#cce5ff',
    secondaryColorRgba: 'rgba(84,91,9, 1)',
    secondaryColorLight: '#cdd1d4',
    successColorRgba: 'rgba(27,185,154, 1)',
    sucessColorLight: '#d9faf3',
    dangerColorRgba: 'rgba(255,57,31, 1)',
    dangerColorLight: '#ffd5d0',
    warningColorRgba: 'rgba(211,158,0, 1)',
    warningColorLight: '#ffefbf',
    columbiaBlueColorRgba: 'rgba(37,167,203, 1)',
    columbiaBlueColorLight: '#d1eff7',
    silverColorRgba: 'rgba(226,230,234, 1)',
    silverColorLight: '#f9fafa',
    steelColorRgba: 'rgba(29,33,36, 1)',
    steelColorLight: '#e2e5e7',
    whiteColorRgba: 'rgba(255,255,255, 1)',
    defaultFontColor: '#212529'
}

var lineData = {
    type: 'line',
    data: {
        labels: [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23
        ],
        datasets : [{
            label: "進線時段",
            data: [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                0,
                1,
                0,
                0,
                0,
                0,
                1,
                0
            ],
            borderColor: lineChartColor.successColorRgba,
            pointBackgroundColor: lineChartColor.whiteColorRgba,
            pointBorderColor: lineChartColor.successColorRgba,
            pointBorderWidth: 3,
            pointRadius: 4,
            fill: true,
            backgroundColor: lineChartColor.sucessColorLight,
            lineTension: 0
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'top',
            display: true
        },
        scales: {
            xAxes: [{
                ticks: {
                    fontColor: lineChartColor.defaultFontColor
                },
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                ticks: {
                    fontColor: lineChartColor.defaultFontColor
                },
                beginAtZero: true
            }],
            gridLines: {
                display: true
            }
        }
    }
}

var pieData = {
    type: 'doughnut',
    data: {
        datasets: [
            {
                data: [
                    1,
                    2,
                    4,
                    5,
                    10,
                    12,
                    13,
                    20,
                ],
                backgroundColor: [
                    defaultColor.primaryColor,
                    defaultColor.secondaryColor,
                    defaultColor.successColor,
                    defaultColor.dangerColor,
                    defaultColor.warningColor,
                    defaultColor.columbiaBlueColor,
                    defaultColor.silverColor,
                    defaultColor.steelColor
                ]
            }
        ],
        labels: [
            "保健食品",
            "家電A",
            "家電B",
            "家電C",
            "碧兒泉",
            "海洋拉娜",
            "資生堂",
            "其他"
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: '商品類別'
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
}

