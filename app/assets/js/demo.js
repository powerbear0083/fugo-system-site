$(function () {
    deliveryDateTimeSelect();
    twoWayInvoiceSelect();
    addNewPaymentMode();
    selectOtherCreditCard();
    cartSampleDataTable();
    changeDeliveryAddress()
    selecteOtherAddress();
    showUserNameInfoSlideUpEffect();
    closeFixedToolBox();
    openCustomerTodo();
    openCustomerComment();
    datePickerStart();
    scrollFixedHead();
    sampleDataTable();
    tabSwitchEffect('invoiceTab', 'invoiceBlock');
    tabSwitchEffect('slideUpRecommendUserInfoTab', 'slideUpRecommendUserInfoBlock');
    tabSwitchEffect('deliveryTab', 'deliveryBlock');
    tabSwitchEffect('searchPromoTab', 'searchPromoBlock');

    function deliveryDateTimeSelect() {
        $('#deliveryDateTimeSelect input[type=radio]').on('click', function (e) {
            var radioValue = $(this).val()
            $('.deliver__data-time').each(function (index) {
                if (radioValue === $(this).data('deliverDate').toString()) {
                    $(this).siblings('.deliver__data-time').addClass('d-none')
                    $(this).removeClass('d-none')
                }

            });
        });
    }

    function twoWayInvoiceSelect() {
        $('#twoWayInvoiceSelect input[type=radio]').on('click', function (e) {
            var invoiceTypeRadioValue = $(this).val()

            $('.invocie__type').each(function (index) {
                if (invoiceTypeRadioValue === $(this).data('invoiceType').toString()) {
                    $(this).siblings('.invocie__type').addClass('d-none')
                    $(this).removeClass('d-none')
                }

            });
        });
    }

    function addNewPaymentMode() {
        $('#addNewPaymentMode').on('click', function (e) {
            e.preventDefault();

            $('#paymentWrap .payment__fields-box').each(function (index) {

                if ($('#selectPayment').val() == $(this).data('paymentBox')) {
                    $(this).removeClass('d-none')
                }
            })
        })
    }

    function selectOtherCreditCard() {
        $('#paymentCreditBox .payment__fields-credit-display-block').on('click', function (e) {
            if ($(this).hasClass('is--selected')) {
                $(this).removeClass('is--selected');
            } else {
                $('#paymentCreditBox .payment__fields-credit-display-block.is--selected').removeClass('is--selected');
                $(this).addClass('is--selected');
            }
        })
    }

    function cartSampleDataTable() {
        $('#discountListTable').DataTable({
            autoWidth: false,
            paging: false,
            info: false,
        });
        $('#marketingListTable').DataTable({
            autoWidth: false,
            paging: false,
            info: false,
        });

        var styleTable = $('#styleTable').DataTable({
            searching: false,
            paging: false,
            info: false,
        });

        $('#itemTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('is--selected')) {
                $(this).removeClass('is--selected');
            } else {
                $('#itemTable tr.is--selected').removeClass('is--selected');
                $(this).addClass('is--selected');
            }

            $('.select-style__style').removeClass('d-none')
        });


        $('#styleTable tbody').on('click', 'tr', function () {
            if ($(this).hasClass('is--selected')) {
                $(this).removeClass('is--selected');
            } else {
                styleTable.$('tr.is--selected').removeClass('is--selected');
                $(this).addClass('is--selected');
            }
        });
    }

    function changeDeliveryAddress() {
        $('#changeDeliveryAddress').on('click', function (e) {
            e.preventDefault();

            $('#deliverBoxOtherAddress').removeClass('d-none');
        });

        $('#deliverBoxOtherAddress .close__btn').on('click', function (e) {
            e.preventDefault();
            $('#deliverBoxOtherAddress').addClass('d-none');
        })
    }

    function selecteOtherAddress() {
        $('#deliverBoxOtherAddress .card__list-text--other-address input[name="otherAddress"]').on('change', function (e) {
            if (e.target.checked) {
                $(this).closest('ul').siblings().removeClass('is-selected');
                $(this).closest('ul').addClass('is-selected');
            }
        })
    }

    function showUserNameInfoSlideUpEffect() {
        $('#memberExampleStyle1 a').on('click', function (e) {
            e.preventDefault();
            if ($(this).data('userName') === 'name') {
                $('#slideUpWrap').addClass('slide-up--open');
            }
        });

        $('#slideUpWrap > .slide-up__head .slide-up__close').on('click', function (e) {
            e.preventDefault();
            $('#slideUpWrap').removeClass('slide-up--open');
        })
    }

    function tabSwitchEffect(tabId, tabBoxId) {

        if (document.getElementById(tabId)) {
            $('#' + tabId + ' > li  > a').on('click', function (e) {
                e.preventDefault();
                var dataTab = $(this).data('tab');
                $(this).parent('li').siblings('li').children('a').removeClass('active');
                $(this).addClass('active');
                $('#' + tabBoxId + ' > div').each(function (index) {
                    if (dataTab === $(this).data('tabBox')) {
                        $(this).siblings().addClass('d-none');
                        $(this).removeClass('d-none');
                    }
                });

                if (tabId === 'searchPromoTab') {
                    if (dataTab == '1') {
                        $('#promoTable2').addClass('d-none');
                        $('#promoTable1').removeClass('d-none');
                    } else {
                        $('#promoTable1').addClass('d-none');
                        $('#promoTable2').removeClass('d-none');
                    }
                }

                if (tabId === 'searchPromoTab') {
                    if (dataTab == '1') {
                        $('#orderTable3').addClass('d-none');
                        $('#orderTable2').addClass('d-none');
                        $('#orderTable1').removeClass('d-none');
                    } else if(dataTab == '2') {
                        $('#orderTable3').addClass('d-none');
                        $('#orderTable1').addClass('d-none');
                        $('#orderTable2').removeClass('d-none');
                    } else {
                        $('#orderTable2').addClass('d-none');
                        $('#orderTable1').addClass('d-none');
                        $('#orderTable3').removeClass('d-none');
                    }
                }
            })

        }
    }

    function openCustomerTodo() {
        if (document.getElementById('customerTodo')) {
            $('#customerTodo > a').on('click', function (e) {
                e.preventDefault();
                $('#customerComment').removeClass('is--open');
                $('#customerComment .fixed-tool__display-box').addClass('d-none');
                $(this).parent('.fixed-tool__todo').addClass('is--open');
                $(this).siblings('.fixed-tool__display-box').removeClass('d-none');
            });
        }
    }

    function openCustomerComment() {
        if (document.getElementById('customerComment')) {
            $('#customerComment > a').on('click', function (e) {
                e.preventDefault();
                $('#customerTodo').removeClass('is--open');
                $('#customerTodo .fixed-tool__display-box').addClass('d-none');
                $(this).parent('.fixed-tool__comment').addClass('is--open');
                $(this).siblings('.fixed-tool__display-box').removeClass('d-none');
            });
        }
    }

    function closeFixedToolBox() {
        if (document.getElementById('customerComment') || document.getElementById('customerTodo')) {
            $('.fixed-tool__dispaly-close').on('click', function (e) {
                e.preventDefault();
                if ($('#customerComment').hasClass('is--open') || $('#customerTodo').hasClass('is--open')) {
                    $('#customerComment') && $('#customerComment').removeClass('is--open');
                    $('#customerTodo') && $('#customerTodo').removeClass('is--open');
                    $(this).closest('.fixed-tool__display-box').addClass('d-none');
                }
            });
        }
    }

    function datePickerStart() {
        $('#catalogurTimeRange, #tvTimeRange').daterangepicker();
        $('#startDate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
        $('#endDate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });
        $('#deliverDate').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true
        });

    }

    function scrollFixedHead() {

        if (document.getElementById('memberInfoWrap')) {
            var $window = $(window);
            var $memberInfoWrap = $('#memberInfoWrap');
            var _memberInfoWrapOuterWidth = $('#memberInfoWrap').outerWidth();

            $window.on('scroll', function () {
                if ($window.scrollTop() > (260)) {
                    $memberInfoWrap.addClass('is--fixed');
                    $memberInfoWrap.css({
                        width: _memberInfoWrapOuterWidth - 20,
                        right: 20
                    });
                    $('.content-page .content').css({
                        marginTop: 196
                    });
                } else {
                    $memberInfoWrap.removeClass('is--fixed');
                    $memberInfoWrap.removeAttr("style");
                    $('.content-page .content').removeAttr("style");
                }
            });
        }

    }

    function sampleDataTable() {

        if (document.getElementById('orderExample1')) {
            $('#orderExample1').DataTable({
                'ajax': 'assets/data/tableData2.txt',
                'columns': [{
                        data: 'AddToFavorite',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if (meta.row == 1) {
                                    return '<a href="" class="btn btn-sm font-red" title="加入最愛" target="_blank"><i class="fa fa-heart bigfonts"></a>';
                                } else {
                                    return '<a href="" class="btn btn-sm font-dark" title="加入最愛" target="_blank"><i class="fa fa-heart-o bigfonts"></a>';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'SaleNum'
                    },
                    {
                        data: 'ItemName',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if ((meta.row % 2) == 0) {
                                    return '<span class="tag tag-info">K</span>' + data + '';
                                }
                                if ((meta.row % 3) == 1) {
                                    return '<span class="tag tag-info">預</span>' + data + '';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'Delivery',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (Array.isArray(data)) {
                                    return '<span class="tag tag-info">超</span>' + '<span class="tag tag-info">到</span>';
                                } else {
                                    switch (data) {
                                        case '1':
                                            return '<span class="tag tag-info">庫</span>';
                                            break;
                                        case '2':
                                            return '<span class="tag tag-info">廠</span>';
                                            break;
                                    }
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'OrderableQuantity'
                    },
                    {
                        data: 'RapidAmount'
                    },
                    {
                        data: 'Price'
                    },
                    {
                        data: 'NumberOfStages'
                    },
                    {
                        data: 'SalesChannel'
                    },
                    {
                        data: 'MultipleCode'
                    },
                    {
                        data: 'PisCode',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-outline-dark btn-sm" title="查詢" target="_blank">查詢</a>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'ExpectedWarehousingDate'
                    },
                    {
                        data: 'AddToCart',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-danger btn-sm" title="加入購物車" data-toggle="modal" data-target="#itemSelectStyle">加入購物車</a>';
                            }
                            return data;
                        }
                    },
                ],
                searching: false
            });

            $('#orderExample1_length').addClass('d-none')
        }

        if (document.getElementById('orderExample2')) {
            $('#orderExample2').DataTable({
                'ajax': 'assets/data/tableData3.txt',
                'columns': [{
                        data: 'AddToFavorite',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if (meta.row == 1) {
                                    return '<a href="" class="btn btn-sm font-red" title="加入最愛" target="_blank"><i class="fa fa-heart bigfonts"></a>';
                                } else {
                                    return '<a href="" class="btn btn-sm font-dark" title="加入最愛" target="_blank"><i class="fa fa-heart-o bigfonts"></a>';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'SaleNum'
                    },
                    {
                        data: 'ItemName',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if ((meta.row % 2) == 0) {
                                    return ''+ data +'<span class="tag tag-info" style="margin-left: 6px">K</span>';
                                }
                                if ((meta.row % 3) == 1) {
                                    return ''+ data +'<span class="tag tag-info" style="margin-left: 6px">預</span>';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'Delivery',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (Array.isArray(data)) {
                                    return '<span class="tag tag-info">超</span>' + '<span class="tag tag-info">到</span>';
                                } else {
                                    switch (data) {
                                        case '1':
                                            return '<span class="tag tag-info">庫</span>';
                                            break;
                                        case '2':
                                            return '<span class="tag tag-info">廠</span>';
                                            break;
                                    }
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'OrderableQuantity'
                    },
                    {
                        data: 'RapidAmount'
                    },
                    {
                        data: 'Price'
                    },
                    {
                        data: 'NumberOfStages'
                    },
                    {
                        data: 'SalesChannel'
                    },
                    {
                        data: 'MultipleCode'
                    },
                    {
                        data: 'PisCode',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-outline-dark btn-sm" title="查詢" target="_blank">查詢</a>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'ExpectedWarehousingDate'
                    },
                    {
                        data: 'StartDate'
                    },
                    {
                        data: 'EndDate'
                    },
                    {
                        data: 'TvType'
                    },
                    {
                        data: 'AddToCart',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-danger btn-sm" title="加入購物車" data-toggle="modal" data-target="#itemSelectStyle">加入購物車</a>';
                            }
                            return data;
                        }
                    },
                ],
                searching: false
            });

            $('#orderExample2_length').addClass('d-none')
        }

        if (document.getElementById('orderExample3')) {
            $('#orderExample3').DataTable({
                'ajax': 'assets/data/tableData4.txt',
                'columns': [{
                        data: 'AddToFavorite',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if (meta.row == 1) {
                                    return '<a href="" class="btn btn-sm font-red" title="加入最愛" target="_blank"><i class="fa fa-heart bigfonts"></a>';
                                } else {
                                    return '<a href="" class="btn btn-sm font-dark" title="加入最愛" target="_blank"><i class="fa fa-heart-o bigfonts"></a>';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'SaleNum'
                    },
                    {
                        data: 'ItemName',
                        render: function (data, type, row, meta) {
                            if (type === 'display') {
                                if ((meta.row % 2) == 0) {
                                    return '<span class="tag tag-info">K</span>' + data + '';
                                }
                                if ((meta.row % 3) == 1) {
                                    return '<span class="tag tag-info">預</span>' + data + '';
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'Delivery',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                if (Array.isArray(data)) {
                                    return '<span class="tag tag-info">超</span>' + '<span class="tag tag-info">到</span>';
                                } else {
                                    switch (data) {
                                        case '1':
                                            return '<span class="tag tag-info">庫</span>';
                                            break;
                                        case '2':
                                            return '<span class="tag tag-info">廠</span>';
                                            break;
                                    }
                                }

                            }
                            return data;
                        }
                    },
                    {
                        data: 'OrderableQuantity'
                    },
                    {
                        data: 'RapidAmount'
                    },
                    {
                        data: 'Price'
                    },
                    {
                        data: 'NumberOfStages'
                    },
                    {
                        data: 'SalesChannel'
                    },
                    {
                        data: 'MultipleCode'
                    },
                    {
                        data: 'PisCode',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-outline-dark btn-sm" title="查詢" target="_blank">查詢</a>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'ExpectedWarehousingDate'
                    },
                    {
                        data: 'StartDate'
                    },
                    {
                        data: 'EndDate'
                    },
                    {
                        data: 'catlogName'
                    },
                    {
                        data: 'pageNumer'
                    },
                    {
                        data: 'AddToCart',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                return '<a href="" class="btn btn-danger btn-sm" title="加入購物車" data-toggle="modal" data-target="#itemSelectStyle">加入購物車</a>';
                            }
                            return data;
                        }
                    },
                ],
                searching: false
            });

            $('#orderExample3_length').addClass('d-none')
        }

        if (document.getElementById('memberExampleStyle1')) {
            // var memberExampleStyle1Table = $('#memberExampleStyle1');
            // memberExampleStyle1Table.DataTable({
            //     'ajax': 'assets/data/tableData.txt',
            //     'columns': [{
            //             data: 'Label'
            //         },
            //         {
            //             data: 'CustomerId'
            //         },
            //         {
            //             data: 'BeginDate'
            //         },
            //         {
            //             data: 'Name'
            //         },
            //         {
            //             data: 'Gender'
            //         },
            //         {
            //             data: 'Birthday'
            //         },
            //         {
            //             data: 'Level'
            //         },
            //         {
            //             data: 'AssignDate'
            //         },
            //         {
            //             data: 'LastCallDate'
            //         },
            //         {
            //             data: 'ScheduleDate'
            //         },
            //         {
            //             data: 'AdviceProduct'
            //         }
            //     ]
            // });
            var select = $('<select id="test"><option value="">客戶經營</option></select>');
            $('#memberExampleStyle1 tfoot th').each(function (index) {
                var title = $(this).text();
                var thSearchInput = $(this).html('<input type="text" placeholder="輸入' + title + '" />');
                thSearchInput.each(function () {
                    $(this).addClass('sort' + index);
                    $(this).css({
                        padding: 0
                    });
                    $(this).find('[type=text]').css({
                        width: '100%',
                        padding: '6px 4px',
                        border: '1px solid #ced4da'
                    });
                    $('#memberExampleStyle1 .sort0, #memberExampleStyle1 .sort1').css({
                        backgroundColor: '#dee2e6'
                    })
                    $('#memberExampleStyle1 .sort0 input[type=text], #memberExampleStyle1 .sort1 input[type=text]').addClass('d-none')
                    $('#memberExampleStyle1 .sort1').append(select);
                    $('#test').css({
                        width: '100%',
                        padding: '6px 4px',
                        border: '1px solid #ced4da'
                    });

                });
                $('#memberExampleStyle1 thead').append(thSearchInput);

            });


            $('#memberExampleStyle1').DataTable().columns().every(function () {
                var that = this;
                // console.log('----------------', this);
                $('input', this.footer()).on('keyup change', function () {
                    // console.log('----------------that.search()', that.search());
                    // console.log('----------------this.value', this.value);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
            });

            $('#memberExampleStyle1_length').addClass('d-none')

        }

        if (document.getElementById('memberExampleStyle2')) {
            var memberExampleStyle2 = $('#memberExampleStyle2').DataTable({
                'ajax': 'assets/data/tableData.txt',
                'columns': [{
                        data: 'Label'
                    },
                    {
                        data: 'CustomerId'
                    },
                    {
                        data: 'BeginDate'
                    },
                    {
                        data: 'Name'
                    },
                    {
                        data: 'Gender'
                    },
                    {
                        data: 'Birthday'
                    },
                    {
                        data: 'Level'
                    },
                    {
                        data: 'AssignDate'
                    },
                    {
                        data: 'LastCallDate'
                    },
                    {
                        data: 'ScheduleDate'
                    },
                    {
                        data: 'AdviceProduct'
                    }
                ],
                'paging': false,
                "scrollY": "350px"
            });


            $('#tableSortExample').on('click', function (e) {
                e.preventDefault();
                // Get the column API object
                var column = memberExampleStyle2.column($(this).attr('data-column'));
                // Toggle the visibility
                column.visible(!column.visible());
            });
        }

        if (document.getElementById('memberExampleStyle3')) {

            var select = $('<select id="test1"><option value="">客戶經營</option></select>');
            $('#memberExampleStyle3 tfoot th').each(function (index) {
                var title = $(this).text();
                var thSearchInput = $(this).html('<input type="text" placeholder="輸入' + title + '" />');
                thSearchInput.each(function () {
                    $(this).addClass('sort' + index);
                    $(this).css({
                        padding: 0
                    });
                    $(this).find('[type=text]').css({
                        width: '100%',
                        padding: '6px 4px',
                        border: '1px solid #ced4da'
                    });
                    $('#memberExampleStyle3 .sort0, #memberExampleStyle3.sort1').css({
                        backgroundColor: '#dee2e6'
                    })
                    $('#memberExampleStyle3 .sort0 input[type=text], #memberExampleStyle3 .sort1 input[type=text]').addClass('d-none')
                    $('#memberExampleStyle3 .sort1').append(select);
                    $('#test1').css({
                        width: '100%',
                        padding: '6px 4px',
                        border: '1px solid #ced4da'
                    });

                });
                $('#memberExampleStyle3 thead').append(thSearchInput);

            });


            $('#memberExampleStyle3').DataTable().columns().every(function () {
                var that = this;
                // console.log('----------------', this);
                $('input', this.footer()).on('keyup change', function () {
                    // console.log('----------------that.search()', that.search());
                    // console.log('----------------this.value', this.value);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
            });

            $('#memberExampleStyle3_length').addClass('d-none')

        }

        if (document.getElementById('slideUpRecommendUserNote')) {
            $('#slideUpRecommendUserNote').DataTable({
                "paging": false,
                "ordering": false,
                "info": false
            });
        }


    }

    var scvLineCtx = document.getElementById("scvLineCart").getContext('2d');
    var scvPieCtx = document.getElementById("scvPieCart").getContext('2d');
    var scvLineChart = new Chart(scvLineCtx, lineData);
    var scvPieCart = new Chart(scvPieCtx, pieData);

});