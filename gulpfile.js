// Plugins variable
const gulp = require('gulp');
const buffer = require('vinyl-buffer');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const spritesmash = require('gulp-spritesmash');
const spritesmith = require('gulp.spritesmith');
const path = require('path');
const hydra = require('gulp-hydra');
const imagemin = require('gulp-imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const pngquant  = require('imagemin-pngquant');
const merge2 = require('merge2')
const pugInheritance = require('gulp-pug-inheritance');
const pug = require('gulp-pug');
const changed = require('gulp-changed');
const cached = require('gulp-cached');
const gulpif = require('gulp-if');
const filter = require('gulp-filter');
const deporder = require('gulp-deporder');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const del = require('del');

Object.defineProperty(Date.prototype, 'YYYYMMDDHHMMSS', {
    value: function () {
        function pad2(n) {  // always returns a string
            return (n < 10 ? '0' : '') + n;
        }

        return this.getFullYear() +
            pad2(this.getMonth() + 1) +
            pad2(this.getDate()) +
            pad2(this.getHours()) +
            pad2(this.getMinutes()) +
            pad2(this.getSeconds());
    }
});

const timestamp = new Date().YYYYMMDDHHMMSS();
const isWindows = /^win/.test(process.platform);

// Compile sass into css
gulp.task('sass', () => {
	return gulp.src(('app/sass/**/*.+(scss|sass)'), { base: 'app/sass/' } )
        .pipe(changed('app/sass/**/*.+(scss|sass)', { hasChanged: changed.compareLastModifiedTime }))
        .pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
		.pipe(autoprefixer({
			browsers: [ 'ie 8-9', 'last 2 versions' ]
		}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('app/assets/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('html', () => {
    gulp.src('app/**/*.html')
        .pipe(changed('app/**/*.html', { hasChanged: changed.compareLastModifiedTime }))
        .pipe(browserSync.reload({
            stream: true
        }));
});


// write js 
gulp.task('js', () => {
    gulp.src('app/script/**/*.js')
        .pipe(changed('app/script/**/*.js', { hasChanged: changed.compareLastModifiedTime }))
        .pipe(gulp.dest('app/assets/js/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// write data
gulp.task('data', () => {
    gulp.src('app/data/**/*.+(js|txt|json)')
        .pipe(changed('app/data/**/*.+(js|txt|json)', { hasChanged: changed.compareLastModifiedTime }))
        .pipe(gulp.dest('app/assets/data/'))
        .pipe(browserSync.reload({
            stream: true
        }));
});


// Start browserSync server
gulp.task('browserSync', () => {
  browserSync({
    server: {
      baseDir: 'app'
    },
    browser: isWindows ? 'chrome' : 'google chrome', 
    port: 1116
  })
});

// Watch all file
gulp.task('watch', [ 'browserSync', 'sass', 'html', 'js', 'data' ], () => {
    gulp.watch('app/sass/**/*.+(scss|sass)', ['sass']);
    gulp.watch('app/**/*.html', ['html']); 
    gulp.watch('app/data/**/*.+(js|txt|json)', ['data']); 
    gulp.watch('app/script/**/*.js', ['js']); 
	gulp.watch('app/*.html').on('change', browserSync.reload);
});


gulp.task( 'default', ['watch'] );

gulp.task( 'build' , () => {

    gulp.src('app/*.html')
        .pipe(gulp.dest('dist/fago-v' + timestamp + ''));
    gulp.src('app/assets/css/**/*.+(css|.map)')
        .pipe(gulp.dest('dist/fago-v' + timestamp + '/assets/css/'));
    gulp.src('app/assets/images/**/*.+(png|jpg|gif|ico)')
        .pipe(gulp.dest('dist/fago-v' + timestamp + '/assets/images'));
    gulp.src('app/assets/data/**/*.+(js|txt|json)')
        .pipe(gulp.dest('dist/fago-v' + timestamp + '/assets/js/'));
    gulp.src('app/assets/js/**/*.js')
        .pipe(gulp.dest('dist/fago-v' + timestamp + '/assets/js/'));
});

gulp.task('clean', () => {
    return del([
        'dist/**/*'
    ]);
});
